import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter number of rows:");
		int row = Integer.parseInt(br.readLine());
		for(int i=1;i<=row;i++){
			int num=64+row,num1=96+row;
			for(int j=1;j<=i;j++){
				if(i%2==1)
					System.out.print((char)num1-- + " ");
				else
					System.out.print((char)num-- + " ");
			}
			System.out.println();
		}
	}
}

