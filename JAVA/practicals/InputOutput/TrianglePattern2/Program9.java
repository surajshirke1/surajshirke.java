import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter number of rows:");
		int row = Integer.parseInt(br.readLine());
		char ch='a';
		for(int i=1;i<=row;i++){
			int num=1+row;
			for(int j=1;j<=i;j++){
				if(j%2==1)
					System.out.print(num++ + " ");
				else
					System.out.print(ch++ + " ");
			}
			System.out.println();
		}
	}
}
