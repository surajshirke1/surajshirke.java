import java.util.*;
class Demo{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter Number of rows:");
		int row=sc.nextInt();
		char ch='A';
		for(int i=1;i<=row;i++){
			for(int j=1;j<=i;j++){
				if(i%2==1){
					System.out.print(j+" ");
				}
				else{
					System.out.print(ch+" ");
				}
				ch++;
			}
			System.out.println();
		}
	}
}

