import java.util.*;
class Demo{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		int row=sc.nextInt();
		int num=1;char ch= 'a';
		for(int i=1;i<=row;i++){
			for(int j=1;j<=i;j++){
				if(j%2==1)
					System.out.print(num+" ");
				else
					System.out.print(ch++ +" ");
			}
			num++;
			System.out.println();
		}
	}
}
