import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter number rows:");
		int row=Integer.parseInt(br.readLine());
		int num=65+row;
		for(int i=1;i<=row;i++){
			for(int j=1;j<=i;j++){
				System.out.print((char)num++ +" ");
			}
			System.out.println();
			
		}
	}
}

