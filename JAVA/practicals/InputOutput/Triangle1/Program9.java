import java.util.*;

class Pattern{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		int row=sc.nextInt();
		int num=row;
		int ch=64+row;
		for(int i=1;i<=row;i++){
			int ch1=ch;
			for(int j=1;j<=num;j++){
				System.out.print((char)ch1-- + "\t");
			}
			num--;
			ch--;
			System.out.println();
		}
	}
}
