class ForWhileLoopExample {

    public static void main(String[] args) {

        // For loop
        for (int i = 0; i < 10; i++) {
            System.out.println("i is: " + i);
        }

        // While loop
        int j = 0;
        while (j < 10) {
            System.out.println("j is: " + j);
            j++;
        }

    }

}
