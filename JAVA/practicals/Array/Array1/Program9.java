import java.util.*;
class Demo{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter size of Array:");
		int size=sc.nextInt();
		int arr[]=new int[size];
		for(int i=0;i<size;i++){
			System.out.print("Enter array element:");
			arr[i]=sc.nextInt();
		}
		for(int i=0;i<size;i++){
			if(i%2==0){
				System.out.println(arr[i]+" is an odd indexed element");
			}
		}
		System.out.println();
	}
}
