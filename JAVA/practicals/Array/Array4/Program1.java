import java.util.*;
class Array1{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter size of your array : ");
		int size = sc.nextInt();
		int arr[] = new int[size];
		System.out.print("Enter array elements : ");
		for(int i=0;i<size;i++){
			arr[i]=sc.nextInt();
		}

		int sum =0;
		for(int j =0;j<size;j++){
			sum+=arr[j];
		
		}
		int avg =sum/size;
		System.out.println("Average of elements in array is " +avg);

	}

}
