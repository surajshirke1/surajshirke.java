import java.util.*;
class Array{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter array size:");
		int size=sc.nextInt();
		int arr[]=new int[size];
		System.out.println("Enter array elements:");
		for(int i=0;i<size;i++){
			arr[i]=sc.nextInt();
		}
		int numMin=arr[0];
		for(int i=0;i<size;i++){
			if(arr[i]<numMin){
				numMin=arr[i];
			}
		}
		System.out.println("Minimum number in array:"+numMin);
	}
}



