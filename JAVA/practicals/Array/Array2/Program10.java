import java.util.*;
class Array{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter size:");
		int size=sc.nextInt();
		int arr[]=new int[size];
		System.out.println("Enter array elements:");
		for(int i=0;i<size;i++){
			arr[i]=sc.nextInt();
		}
		int numMax=arr[0];
		for(int i=0;i<size;i++){
			if(arr[i]>numMax){
				numMax=arr[i];
			}
		}
		System.out.println("Max number in array is:"+numMax);
	}
}


