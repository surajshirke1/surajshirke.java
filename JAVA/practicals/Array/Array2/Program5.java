import java.util.*;
class Array{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter array size:");
		int size=sc.nextInt();
		int arr[]=new int[size];
		System.out.println("Enter array elements:");
		int sum=0;
		for(int i=0;i<size;i++){
			arr[i]=sc.nextInt();
			if(i%2==1){
				sum+=arr[i];
			}
		}
		System.out.println("Sum of odd index elements:"+sum);
	}
}



